Luodaan int tyyppinen muuttuja: keilasarja, joka on
10 numeroinen.
N�m� 10 numeroa kertovat heittosarjojen m��r�n ja
kukin sarja on maksimissaan 30 pisteen arvoinen ja
yhden ruudun kokoinen. Maksimissaan saa siis 300 pistett�.
Keilasarjan sis�lle luodaan toinen int muuttuja nimelt��n
ruutu. N�it� ruutuja tarvitaan 10 kappaletta. Jokainen ruutu
on maksimissaan arvoltaan 30 pistett�.
9 ensimm�ist� ruutua sis�lt�� kaksi heittoa ja viimeinen kolme.
Ensin pit�isi kysy� k�ytt�j�lt� montako keilaa kaatui
println komennolla ja ottaa annettu arvo yl�s scanline komennolla.
T�m� scannattu numero, joka on kokonaisluku pit�isi kaapata talteen
setvalue:lla.
Setvalueita voimme ynn�t� toisiinsa tai tarpeen tullen kertoa.
Setvalue voidaan esim tyhjent�� ynn�yksen tms j�lkeen.
Saamme jokaisen ynn�yksen j�lkeen esim lis�tty� 1 muuttujaan,joka 
menness��n numeroon 12 sulkee kyselyn ja tulostaa kokonais
pistem��r�n. T�m�n j�lkeen voimme joko aloittaa alusta tai lopettaa.


